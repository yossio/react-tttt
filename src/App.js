import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Restaurant from './components/Restaurant';
import AddRestaurant from './components/AddRestaurant';

class App extends Component {
 
    componentDidMount()
    {
       this.refreshRestsFromServer()
    } 


    refreshRestsFromServer()
    {
         //comp mounted , after render 
        fetch("http://localhost:3000/rests")
        .then(res=> res.json())
        .then(data=>
             {
              var myResultObj={
               allRests:data
            }
            this.setState(myResultObj);
        }) 
    }
 
    constructor(props)
    {
        super(props);
        this.state={
            allRests:[]
        } 
    } 
 
  render() {
    return (
      <div className="container">
         <div className="row">
            <h1>All Restaurants</h1>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>
                        NAME
                    </th>
                    <th>
                        RATING
                    </th>
                </tr>
            </thead>
          <tbody>
            {
                this.state.allRests.map(rest=> 
                     <Restaurant   currentRest={rest}    key={rest.id}      />   )
            } 
          </tbody> 
            </table>
         </div> 


        <div className="row">
            <AddRestaurant 
             refreshAppData={this.refreshRestsFromServer.bind(this)}    /> 
        </div>



      </div>
    );
  }
}

export default App;
