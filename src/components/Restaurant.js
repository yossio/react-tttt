import React, { Component } from 'react';
 

class Restaurant extends Component {
  render() {
    return (
        <tr>
                <td>
                    {this.props.currentRest.name}
                </td>
                <td>
                    {this.props.currentRest.rating}
                </td>
        </tr>
    );
  }
}

export default Restaurant;
