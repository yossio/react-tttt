import React, { Component } from 'react';
 

class AddRestaurant extends Component {
  render() {
    return (
        <div>
            <h1>Add restaurant</h1>
            <input placeholder="name"  id="myName"  /><br/>
            <input placeholder="rating" id="myRating"  /> 
            <button onClick={this.addRest.bind(this)} >add</button> 
        </div>
    );
  } 
  addRest()
  {
     var restNameToCreate=document.getElementById("myName").value;
     var ratingToCreate=document.getElementById("myRating").value;

     var objToSend={
         name:restNameToCreate,
         rating:ratingToCreate
     }

     fetch('http://localhost:3000/rests', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(objToSend )
      }).then(resp=>resp.json())
      .then(data=>
        {
            //after add rest 
            //data recived from server
            this.props.refreshAppData(); 
        });

  }


}

export default AddRestaurant;
